{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NegativeLiterals #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveFunctor #-}

module Lib
    ( someFunc
    ) where

import Prelude hiding ((==), (>=), (<), (>), (<=))
import qualified Data.List as L
import qualified Prelude as P
import Control.Monad.ST
import Control.Monad.STE
import Data.STRef
import Control.Monad
import Control.Monad.Trans
import Control.Applicative
import qualified Control.Monad.Loop as L
import Control.Monad.Trans.Control
import Data.Primitive hiding (fromList)
import Data.Kind
import Data.Coerce
import Data.Matrix hiding (transpose)
import qualified Data.Matrix as M
import Debug.Trace
-- import NumericPrelude hiding ((==), (>=), (<), (>), (<=))
-- import MathObj.Matrix hiding (transpose)
-- import qualified MathObj.Matrix as M


newtype MutableVar s a =
  MutableVar { unMut :: ((STRef s a)) }

define :: a -> ST s (MutableVar s a)
define a = MutableVar <$> newSTRef a

copy :: MutableVar s a -> MutableVar s a
copy = liftA id

(=:) :: MutableVar s a -> MutableVar s a -> ST s ()
MutableVar sa =: MutableVar newARef = do
    newA <- readSTRef newARef
    writeSTRef sa newA

-- loop :: MutableVar s a -> MutableVar s ()
-- loop Break = pure ()
-- loop _ = error "Infinite loop"

-- instance (MonadTrans t, MonadTransControl t, Monad m, Mutable m) => Mutable (t m) where
--     define a = lift $ define a
--     copy a = undefined $ liftWith (\r -> copy (r a))
--     b =: a = undefined
  
infixl 0 =:

type family MyEqResult a where
  MyEqResult (MutableVar s _) = MutableVar s Bool
  MyEqResult _ = Bool

class MyEq a where
  (==) :: a -> a -> MyEqResult a
  (!=) :: a -> a -> MyEqResult a

instance {-# OVERLAPPING #-} (Eq a, MyEqResult a ~ Bool) => MyEq a where
  (==) = (P.==)
  (!=) = (P./=)
  
class MyOrd a where
  (<) :: a -> a -> MyEqResult a
  (<=) :: a -> a -> MyEqResult a
  (>) :: a -> a -> MyEqResult a
  (>=) :: a -> a -> MyEqResult a

instance {-# OVERLAPPING #-} (Ord a, MyEqResult a ~ Bool) => MyOrd a where
  (<) = (P.<)
  (<=) = (P.<=)
  (>) = (P.>)
  (>=) = (P.>=)

-- instance Functor (MutableVar s) where
--   fmap f (MutableVar s) = MutableVar $ liftA f (s >>= readSTRef) >>= newSTRef

-- instance Applicative (MutableVar s) where
--   pure a = MutableVar (newSTRef a)
--   MutableVar sf <*> MutableVar sa = MutableVar $ do
--     f <- sf >>= readSTRef
--     a <- sa >>= readSTRef
--     newSTRef $ f a

-- instance Monad (MutableVar s) where
--   return = pure
--   MutableVar sa >>= msf = MutableVar $ do
--     a <- sa >>= readSTRef
--     b <- unMut (msf a) >>= readSTRef
--     newSTRef b

instance P.Num a => P.Num (MutableVar s a) where
  (+) = liftA2 (+)
  (*) = liftA2 (*)
  fromInteger = define . fromInteger
  
instance P.Fractional a => P.Fractional (MutableVar s a) where
  (/) = liftA2 (/)
  fromRational = pure . fromRational

instance {-# OVERLAPPING #-} (MyEq a, MyEqResult a ~ Bool) => MyEq (MutableVar s a) where
  (==) = liftA2 (==)
  (!=) = liftA2 (!=)
  
instance {-# OVERLAPPING #-} (MyOrd a, MyEqResult a ~ Bool) => MyOrd (MutableVar s a) where
  (<) = liftA2 (<)
  (<=) = liftA2 (<=)
  (>) = liftA2 (>)
  (>=) = liftA2 (>=)

transpose :: MutableVar s (Matrix Double) -> MutableVar s (Matrix Double)
transpose = liftA M.transpose

unwrap :: MutableVar s (Matrix Double) -> MutableVar s Double
unwrap m = (! (1, 1)) <$> m

(.*) :: Num a => MutableVar s a -> MutableVar s (Matrix a) -> MutableVar s (Matrix a)
a .* ma = liftA2 scaleMatrix a ma

while :: Monad m => m Bool -> m a -> m ()
while cond action =
  traceM "In While" >> cond >>= \c -> when c $ action >> while cond action

cgm :: Matrix Double -> Matrix Double -> Matrix Double -> MutableVar s (Matrix Double)
cgm b a x_0 = do
  let x_k = define x_0
      epsilon = define 1
      r_k = define $ b - a * x_0
      p_k = copy r_k
  r_k >>= traceShowM

  while (unwrap (transpose r_k * r_k) >= epsilon) $ do
    let alpha_k = unwrap (transpose r_k * r_k) / unwrap (transpose p_k * pure a * p_k)
        x_k1 = x_k + alpha_k .* p_k
        r_k1 = r_k - alpha_k .* pure a * p_k
        beta_k = unwrap (transpose r_k1 * r_k1) / unwrap (transpose r_k * r_k)
        p_k1 = r_k1 + beta_k .* p_k
    x_k >>= traceShowM
    x_k1 >>= traceShowM
    x_k =: x_k1
    x_k >>= traceShowM
    r_k =: r_k1
    p_k =: p_k1

  x_k

runMutVar :: forall a. (forall s. MutableVar s a) -> a
runMutVar ma = runST $ unMut ma >>= readSTRef

someFunc :: IO ()
someFunc = print $ runMutVar $ cgm (fromList 3 1 [10, 10, 10]) (fromList 3 3 [1, 2, 3, 4, 5, 6, 7, 8, 9]) (fromList 3 1 [0, 0, 0])
